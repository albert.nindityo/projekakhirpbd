<?php

use App\Http\Controllers\MoviesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TicketsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('Movies.dashboard');
// });


Route::get('/home',[MoviesController::class,'index']);
Route::get('/add',[MoviesController::class,'create'])->name('add');
Route::post('/store',[MoviesController::class,'store'])->name('store');
Route::get('/list', [MoviesController::class,'show'])->name('list');
Route::get('/edit/{id}', [MoviesController::class,'edit'])->name('edit');
Route::put('/update/{id}', [MoviesController::class,'update'])->name('update');
Route::get('/destroy/{id}', [MoviesController::class,'destroy'])->name('destroy');


Route::get('/book/{id}', [TicketsController::class, 'create'])->name('book');
Route::get('/showTicket', [TicketsController::class, 'show'])->name('showTickets');
Route::get('/hancur/{id}', [TicketsController::class, 'destroy'])->name('delTicket');

Route::get('/tickets', [TicketsController::class, 'index'])->name('tickets.index');
Route::post('/tickets/{id}', [TicketsController::class, 'store'])->name('tickets.store');
