@extends('Include.sidebar-Dashboard')
@section('content')
@include('sweetalert::alert')

<div class="container">
    <div class="grid">
        @foreach ($movies as $movie)
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{ $movie->images }}" alt="Card image cap">
                    <h5 class="card-title">{{ $movie->title }}</h5>
                    <div class="card-body">
                        <p class="card-text">{{ $movie->description }}</p> 
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Duration: {{ $movie->duration }} Minutes</li>
                        <li class="list-group-item">Seat: {{ $movie->seat }} </li>
                        <li class="list-group-item">Action :
                            <a href="{{ route('book', $movie->id_movies) }}" class="btn btn-primary">book</a>
                        </li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection