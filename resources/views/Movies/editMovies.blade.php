@extends('Include.sidebar-TableShow')
@section('content')
@include('sweetalert::alert')

<div class="content">
    <div class="row">
        <form action="{{ route('update', $movies->id) }}" method="POST">
            @csrf
            @method('PUT')
            <label for="title">Title:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input type="text" name="title" value="{{ $movies->title }}" required><br>
        
            <label for="description">Description:</label>
            <textarea name="description" required>{{ $movies->description }}</textarea><br>
        
            <label for="duration">Duration:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input type="text" name="duration" value="{{ $movies->duration }}" required><br>
        
            <label for="images">Images: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input type="text" name="images" value="{{ $movies->images }}" required><br>

            <label for="seat">Total Seat:&nbsp;&nbsp;&nbsp;&nbsp;</label>
            <input type="text" name="seat" value="{{ $movies->seat }}" required><br><br>

            <input class="btn btn-primary" type="submit" value="Update Movie">
        </form>
    </div>
</div>

@endsection