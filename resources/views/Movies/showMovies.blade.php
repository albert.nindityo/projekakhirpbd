@extends('Include.sidebar-TableShow')
@section('content')
@include('sweetalert::alert')

    <div class="content">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Title</th>
                    <th scope="col">Description</th>
                    <th scope="col">Duration</th>
                    <th scope="col">Seat</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($movies as $movie)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $movie->title }}</td>
                        <td>{{ $movie->description }}</td>
                        <td>{{ $movie->duration }}</td>
                        <td>{{ $movie->seat }}</td>
                        <td><a href="{{ route('edit', $movie->id_movies) }}" class="btn btn-primary">Edit</a></td>
                        <td><a href="{{ route('destroy', $movie->id_movies) }}" class="btn btn-danger">Delete</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
