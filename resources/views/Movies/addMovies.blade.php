@extends('Include.sidebar-Add')
@section('content')
@include('sweetalert::alert')

<div class="content">
    <div class="column">
        <form action="{{ route('store') }}" method="POST"  enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleFormControlInput1">Movie Title</label>
              <input type="text" class="form-control" name="title" placeholder="">
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control" name="description" rows="3"></textarea>
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput1">Duration</label>
                <input type="text" class="form-control" name="duration" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput1">Image Link</label>
                <input type="text" class="form-control" name="images" placeholder="">
              </div>
              <div class="form-group">
                <label for="exampleFormControlInput1">Total Seat</label>
                <input type="text" class="form-control" name="seat" placeholder="">
              </div>

              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
    </div>

</div>

@endsection
