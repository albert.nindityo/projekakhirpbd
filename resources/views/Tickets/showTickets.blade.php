@extends('Include.sidebar-TableTicket')
@section('content')
@include('sweetalert::alert')

<div class="content">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">No</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Quantity</th>
                <th scope="col">Title</th>

            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $ticket->name }}</td>
                    <td>{{ $ticket->email }}</td>
                    <td>{{ $ticket->quantity }}</td>
                    <td>{{ $ticket->movie->title}}</td>
                    <td><a href="{{ route('delTicket', $ticket->id_tickets) }}" class="btn btn-danger">Delete</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
