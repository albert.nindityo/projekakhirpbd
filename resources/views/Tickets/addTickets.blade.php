@extends('Include.sidebar-Dashboard')
@section('content')
@include('sweetalert::alert')

<div class="content">
    <div class="container">
        <h1>Beli Tiket - {{ $movies->title }}</h1>
        <form action="{{ route('tickets.store', $movies->id_movies) }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="quantity">Jumlah Tiket</label>
                <input type="number" class="form-control" id="quantity" name="quantity" required min="1">
            </div>
            <button type="submit" class="btn btn-primary">Book</button>
        </form>
    </div>
</div>

@endsection