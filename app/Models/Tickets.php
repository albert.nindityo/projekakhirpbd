<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Movies;

class Tickets extends Model
{
    use HasFactory;
    protected $primaryKey = 'id_tickets';

    protected $fillable =[
        'name',
        'email',
        'quantity',
        'title',
     ];

    /**
     * Get the movie associated with the ticket.
     */
    public function movie()
    {
        return $this->belongsTo(Movies::class, 'id_movies');
    }
}
