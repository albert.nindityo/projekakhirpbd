<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movies extends Model
{
    
    use HasFactory;
    protected $primaryKey = 'id_movies';

    protected $fillable =[
       'title',
       'description',
       'duration',
       'images',
       'seat'
    ];

protected $increment = True;
}
