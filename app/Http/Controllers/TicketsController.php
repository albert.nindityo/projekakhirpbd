<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movies;
use App\Models\Tickets;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;


class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tickets = Tickets::with('movie')->get();
        return view('Tickets.addTickets', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create($id_movies)
    {
        $movies = Movies::findOrFail($id_movies);
        return view('Tickets.addTickets', compact('movies'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $id_movies)
    {
        $movie = Movies::find($id_movies);
        if ($request->input('quantity') > $movie->seat) {
            Alert::error('Maaf', 'Jumlah seat melebihi kuota');
            return redirect()->back();
        } else {

            $name = $request->name;
            $email = $request->email;
            $quantity = $request->quantity;
            $id_movies = $movie->id_movies;

            // Memanggil stored procedure untuk memuat data 
            $query = "CALL sp_insert_tickets(?, ?, ?, ?)";
            $bindings = [$id_movies, $name, $email, $quantity];

            DB::statement($query, $bindings);
            // return redirect('list', compact('movies'));
        }
        return redirect('/showTicket');
    }
    /**
     * Display the specified resource.
     */
    public function show()
    {
        $tickets = Tickets::with('movie')->get();
        return view('Tickets.showTickets', compact('tickets'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id_movies)
    {
        $ticket = Tickets::findOrFail($id_movies);
        $ticket->delete();

        return redirect('showTicket');
    }
}
