<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movies;
use Illuminate\Support\Facades\DB;

class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $movies = Movies::all();
        return view('Movies.dashboard', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $movies = Movies::all();
        return view('Movies.addMovies', compact('movies'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'duration' => 'required',
            'images' => 'required',
            'seat' => 'required',
        ]);

        $title = $request->title;
        $description = $request->description;
        $duration = $request->duration;
        $images = $request->images;
        $seat = $request->seat;

        // Memanggil stored procedure untuk memuat data 
        $query = "CALL sp_insert_movie(?, ?, ?, ?, ?)";
        $bindings = [$title, $description, $duration, $images, $seat];

        DB::statement($query, $bindings);
        // return redirect('list', compact('movies'));

        $movies = Movies::all();
        return redirect('list');
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $movies = [
            'movies' => Movies::all()
        ];
        return view('Movies.showMovies', $movies);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id_movies)
    {
        $movies = Movies::findorfail($id_movies);

    return view('Movies.editMovies', compact('movies'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, String $id_movies)
    {

        $movies = Movies::findorfail($id_movies);
        $movies->update($request->all());
        return redirect('list');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id_movies)
    {
        $movies = Movies::findOrFail($id_movies);
        $movies->delete();
    
        return redirect('list');
    }
}
